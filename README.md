# Pirati Style Guide 2

This repository holds second version of UI styleguide for Czech Pirate Party websites. It's
intention is to made a core framework to be used across all sites and build all
UI using it. This will ensure better consistency a easier updates.

It is built upon:

- [Django](https://github.com/django/django) is a high-level Python web framework that encourages rapid development and clean, pragmatic design
- [Vite](https://vitejs.dev/) is a build tool that aims to provide a faster and leaner development experience for modern web projects
- [Django Vite](https://github.com/MrBin99/django-vite) is Integration of ViteJS in a Django project.
- [Django pattern library](https://github.com/torchbox/django-pattern-library) is a tool for developing design systems in django ecosystem
- [Tailwind CSS](https://tailwindcss.com/) is a brilliant utility-first CSS framework built on top of PostCSS


## Getting Started

These instructions are for getting the project running off a local development environment:

- Install any system requirements on your dev environment
- Use NPM to install project dependencies and build the project


### System Requirements

- [Node.js](https://nodejs.org) (v14.5.x or later)
- [Python](https://www.python.org/) (v3.10 or later)


### Install and Run

It's JavaScript and Python all the way down so once you have the the system requirements
you can run the following commands:

### Setup Django APP

#### Install requirements
```
pip install -r requirements/base.txt
```

#### Migrate core apps
```
python manage.py migrate --settings=majak_uistyleguide.settings.dev
```

#### Run server
```
python manage.py runserver --settings=majak_uistyleguide.settings.dev
```

### Setup Vite

#### Install requirements
```
npm i
```

### Dev mode (with setting DJANGO_VITE_DEV_MODE = True)
cp majak_uistyleguide/settings/local.py.example majak_uistyleguide/settings/local.py
npm run dev
python manage.py runserver --settings=majak_uistyleguide.settings.local

### Build

The build command compiles the styleguide CSS To build, run:
```
npm run prod
```


#### Collect static
Run npm command before collecting static files
```
npm run prod
python manage.py collectstatic --settings=majak_uistyleguide.settings.dev
```
