from pathlib import Path
import environ


ROOT_DIR = Path(__file__).parents[2]
PROJECT_DIR = ROOT_DIR / "majak_uistyleguide"

env = environ.Env()
environ.Env.read_env(str(ROOT_DIR / ".env"))


# GENERAL
# ------------------------------------------------------------------------------
DEBUG = env.bool("DJANGO_DEBUG", False)
ROOT_URLCONF = "majak_uistyleguide.urls"
WSGI_APPLICATION = "majak_uistyleguide.wsgi.application"

# I18N and L10N
# ------------------------------------------------------------------------------
LANGUAGE_CODE = 'cs'
TIME_ZONE = "Europe/Prague"
USE_I18N = True
USE_TZ = True

# DATABASES
# ------------------------------------------------------------------------------
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ROOT_DIR / 'db.sqlite3',
    }
}
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# APPS
# ------------------------------------------------------------------------------
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_vite',
    'pattern_library',
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# TEMPLATES
# ------------------------------------------------------------------------------
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [str(PROJECT_DIR / "templates")],
        "OPTIONS": {
            "loaders": [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ],
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
            "builtins": [
                "pattern_library.loader_tags"
            ],
            "libraries": {
                "random": "majak_uistyleguide.templatetags.random"
            }
        },
    },
]


# STATIC
# ------------------------------------------------------------------------------
STATIC_URL = "/static/"
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

# MEDIA
# ------------------------------------------------------------------------------
MEDIA_URL = "/media/"
MEDIA_ROOT = str(ROOT_DIR / "media_files")

# VITE SETTINGS
# ------------------------------------------------------------------------------
# Where ViteJS assets are built.
DJANGO_VITE_ASSETS_PATH = ROOT_DIR / 'dist'
STATIC_FILES = PROJECT_DIR / 'static'

# If use HMR or not.
DJANGO_VITE_DEV_MODE = False

# Name of static files folder (after called python manage.py collectstatic)
STATIC_ROOT = PROJECT_DIR / "collectedstatic"

# Include DJANGO_VITE_ASSETS_PATH into STATICFILES_DIRS to be copied inside
# when run command python manage.py collectstatic
SRC_PATH = ROOT_DIR / 'src'
STATICFILES_DIRS = [DJANGO_VITE_ASSETS_PATH, STATIC_FILES, SRC_PATH]

# PATTERN LIBRARY SETTINGS
# ------------------------------------------------------------------------------
if DEBUG:
    X_FRAME_OPTIONS = "SAMEORIGIN"

PATTERN_LIBRARY = {
    # Groups of templates for the pattern library navigation. The keys
    # are the group titles and the values are lists of template name prefixes that will
    # be searched to populate the groups.
    "SECTIONS": (
        ("atoms", ["patterns/atoms"]),
        ("molecules", ["patterns/molecules"]),
        ("organisms", ["patterns/organisms"]),
        ("templates", ["patterns/templates"]),
    ),

    # Configure which files to detect as templates.
    "TEMPLATE_SUFFIX": ".html",

    # Set which template components should be rendered inside of,
    # so they may use page-level component dependencies like CSS.
    "PATTERN_BASE_TEMPLATE_NAME": "patterns/base.html",

    # Any template in BASE_TEMPLATE_NAMES or any template that extends a template in
    # BASE_TEMPLATE_NAMES is a "page" and will be rendered as-is without being wrapped.
    "BASE_TEMPLATE_NAMES": ["patterns/base_page.html"],
}
