from uuid import uuid4

from django import template

register = template.Library()


@register.simple_tag
def random_uuid():
    return str(uuid4())
