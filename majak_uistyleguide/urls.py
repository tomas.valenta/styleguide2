from django.urls import path, include

urlpatterns = [
    path("", include("pattern_library.urls")),
]
