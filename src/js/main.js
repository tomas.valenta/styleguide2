import '../css/style.pcss';

import Vue from "vue";
import { forEachNode } from "./utils";

import ArticleCarousel from "./components/articles/ArticleCarousel";
import HeaderCarousel from "./components/header/HeaderCarousel";
import Renderer from "./components/calendar/Renderer";
import DummyProvider from "./components/calendar/DummyProvider";
import GoogleProvider from "./components/calendar/GoogleProvider";
import RegionMap from "./components/RegionMap";
import TwitterCarousel from "./components/TwitterCarousel";
import ViewProvider from "./components/ViewProvider";
import Navbar from "./components/navbar/Navbar";
import FooterCollapsible from "./components/footer/FooterCollapsible";
import FlipClock from "./components/FlipClock";
import HorizontalScrollable from "./components/HorizontalScrollable";

import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';


Vue.component("ui-article-carousel", ArticleCarousel);
Vue.component("ui-header-carousel", HeaderCarousel);
Vue.component("ui-calendar-renderer", Renderer);
Vue.component("ui-calendar-dummy-provider", DummyProvider);
Vue.component("ui-calendar-google-provider", GoogleProvider);
Vue.component("ui-region-map", RegionMap);
Vue.component("ui-twitter-carousel", TwitterCarousel);
Vue.component("ui-view-provider", ViewProvider);
Vue.component("ui-navbar", Navbar);
Vue.component("ui-footer-collapsible", FooterCollapsible);
Vue.component("ui-flip-clock", FlipClock);
Vue.component("ui-horizontal-scrollable", HorizontalScrollable);


import UiApp from "./components/UiApp.vue";



const appFactory = (el, attrs) => {
  // Bootstrap Vue.js.
  new Vue({
    el,
    components: {
      UiApp
    }
  });
};


/**
 * Bootstrap Vue.js application at given Element instance.
 *
 * App properties are passed via data attributes, like:
 *
 * <div class="__vue-root" data-message="Hello" data-app="SomeApp"></div>
 *
 * @param {Element} el DOM Element
 */
const renderVueAppElement = el => {
  const attrs = Object.assign({}, el.dataset);
  return appFactory(el, attrs);
}

/**
 * Add a Tippy.js tooltip to the given element, containing its ARIA label.
 * 
 * @param {Element} el DOM Element
 */
const addTooltip = el => {
  tippy(
    el,
    {content: el.getAttribute("aria-label")}
  )
}


const init = event => {
  // Initialize Vue.js apps.
  forEachNode(document.querySelectorAll('.__js-root'), renderVueAppElement);
  forEachNode(document.querySelectorAll('.__tooltip'), addTooltip);
}

document.addEventListener('DOMContentLoaded', init);
