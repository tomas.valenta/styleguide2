export const forEachNode = function (array, callback, scope) {
  for (var i = 0; i < array.length; i++) {
    callback.call(scope, array[i]); // passes back stuff we need
  }
};

export function getWindowWidth() {
  return Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
}

export function isLgScreenSize() {
  return getWindowWidth() >= 1200;
}
