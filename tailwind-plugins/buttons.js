const _ = require('lodash');
const Color = require('color');
// const defaultConfig = require('tailwindcss/defaultConfig')();

const darken = (clr, val) => Color(clr).darken(val).rgb().string();
const lighten = (clr, val) => Color(clr).lighten(val).rgb().string();

function defaultOptions() {
  return {
    textAlign: 'center',
    display: 'inline-block',
    fontWeight: '400',
    maxWidth: '20rem',
    colors: {},
  }
}

module.exports = function (options) {
  options = _.isFunction(options)
    ? options(defaultOptions())
    : _.defaults(options, defaultOptions());

  return function ({ addComponents, addUtilities, e, theme }) {
    const getThemeColor = (alias, fallback) => {
      if (!alias) {
        return fallback;
      }
      return theme(`colors.${alias}`);
    }

    const btnUtilities = {
      '.btn.btn--fullwidth': {
        width: '100%',
        maxWidth: '100%',
        '.btn__body-wrap': {
          width: '100%',
          maxWidth: '100%',
        },
        '.btn__body': {
          flex: '1'
        }
      },
      '.btn.btn--autowidth': {
        width: 'auto',
      }
    };

    addUtilities(btnUtilities, ['responsive']);

    const defaultIconBorder = getThemeColor(options.defaultColor.iconBorder, lighten(getThemeColor(options.defaultColor.background, '#000'), 0.1));

    addComponents([
      {
        '.btn': {
          display: 'inline-block',
          textAlign: 'center',
          fontWeight: theme('fontWeight.normal'),
          maxWidth: theme('maxWidth.xs'),
          textDecoration: 'none',
        },
        '.btn[disabled]': {
          opacity: '0.7',
          cursor: 'not-allowed',
        },
        '.btn:hover': {
          textDecoration: 'none',
        },
        '.btn__body': {
          display: 'flex',
          height: '100%',
          alignItems: 'center',
          justifyContent: 'center',
          padding: '.25em 2em'
        },
        '.btn__body-wrap': {
          minWidth: '10rem',
          minHeight: '2.75rem',
        },
        '.btn__icon': {
          display: 'flex',
          alignItems: 'center',
          borderLeftWidth: '1px',
          padding: `0 ${theme('spacing.4')}`,
          borderColor: defaultIconBorder,
        },
        // All possible parts are transition ready
        '.btn__body, .btn__icon, .btn__inline-icon': {
          transitionProperty: 'color,background-color,border-color',
          transitionDuration: '.2s',
          color: getThemeColor(options.defaultColor.text),
        },
        '.btn__body, .btn__icon': {
          backgroundColor: getThemeColor(options.defaultColor.background),
        },
        '.btn__icon img': {
          width: theme('spacing.4')
        },
        // Button with a standalone icon on right side
        '.btn--icon .btn__body-wrap': {
          display: 'flex',
        },
        // Button with a standalone icon on the left
        '.btn--inverted-icon .btn__icon': {
          order: '-1',
          borderRightWidth: '1px',
          borderLeftWidth: '0',
        },
        // Button with an inline icon
        '.btn__inline-icon': {
          marginRight: theme('spacing.2'),
        },
        // Smaller size
        '.btn--condensed .btn__body': {
          padding: '.75em 1em',
        },
        '@keyframes btn-loading-spinner': {
          'to': {
            transform: 'rotate(360deg)',
          }
        },
        '.btn--loading': {
          position: 'relative',
        },
        '.btn--loading::before': {
          pointerEvents: 'none',
          content: "''",
          position: 'absolute',
          left: '0',
          right: '0',
          top: '0',
          bottom: '0',
          zIndex: '2',
          backgroundColor: theme('colors.black'),
          opacity: '0.4',
        },
        '.btn--loading::after': {
          pointerEvents: 'none',
          content: "''",
          position: 'absolute',
          left: '0',
          right: '0',
          top: '0',
          bottom: '0',
          zIndex: '3',
          top: '50%',
          left: '50%',
          width: '1.5rem',
          height: '1.5rem',
          marginTop: '-0.75rem',
          marginLeft: '-0.75rem',
          borderRadius: '50%',
          border: `3px solid ${theme('colors.cyan.200')}`,
          borderLeftColor: 'transparent',
          animation: 'btn-loading-spinner 1s linear infinite',
        }
      },
      ..._.map(options.colors, (colorOptions, name) => {
        const bg = getThemeColor(colorOptions.background, '#000');
        return {
          // Standard colorized variant
          [`.btn--${e(name)} .btn__body, .btn--${e(name)} .btn__icon`]: {
            backgroundColor: getThemeColor(colorOptions.background),
            color: getThemeColor(colorOptions.text),
          },
          [`.btn--${e(name)} .btn__icon`]: {
            borderColor: getThemeColor(colorOptions.iconBorder, darken(bg, 0.1)),
            backgroundColor: getThemeColor(colorOptions.iconBackground, bg),
          },
          // Hover state unless set with --to-* variant.
          [`.btn--${e(name)}.btn--hoveractive:not([class^="btn--to-"]):hover`]: {
            '.btn__body, .btn__icon': {
              backgroundColor: getThemeColor(colorOptions.hoverBackground, darken(bg, 0.2)),
              color:  getThemeColor(colorOptions.hoverText, '#fff'),
            },
            '.btn__icon': {
              borderColor: getThemeColor(
                colorOptions.hoverIconBorder,
                darken(bg, 0.3)
              ),
            },
            '.btn__icon svg, .btn__icon i': {
              color:  getThemeColor(colorOptions.hoverText, '#fff'),
              fill: getThemeColor(colorOptions.hoverText, '#fff')
            }
          },
          [`.btn--hoveractive.btn--to-${e(name)}:hover, .btn--to-${e(name)}.btn--activated`]: {
            '.btn__body': {
              backgroundColor: `${getThemeColor(colorOptions.background)} !important`,
              color: `${getThemeColor(colorOptions.text)} !important`,
            },
            '.btn__icon': {
              borderColor: `${getThemeColor(colorOptions.iconBorder, darken(getThemeColor(colorOptions.background, '#000'), 0.1))} !important`,
              backgroundColor: `${getThemeColor(colorOptions.iconBackground, getThemeColor(colorOptions.background, '#000'))} !important`,
            },
            '.btn__inline-icon': {
              color: `${getThemeColor(colorOptions.text)} !important`,
            }
          }
        }
      })
    ])
  }
}
