module.exports = {
    content: [
        './majak_uistyleguide/static/src/**/*.{html,vue}',
        './majak_uistyleguide/templates/patterns/**/*.html',
    ],
    theme: {
        extend: {
            maxWidth: {
                'xxs': '16rem',
            },
            opacity: {
                '85': '0.85',
            },
            lineHeight: {
                'negative': '0.9',
            },
            spacing: {
                '0/5': '0.125rem',
                '14': '3.5rem',
                '28': '7rem',
                '36': '9rem',
                '44': '11rem',
                '52': '13rem',
                '80': '20rem'
            },
            // flip-x
            scale: {
                '-1': '-1',
            },
        },
        // Breakpoints
        screens: {
            'sm': '576px',
            'md': '768px',
            'lg': '992px',
            'xl': '1200px',
            '2xl': '1366px',
            '3xl': '2060px',
        },
        fontFamily: {
            alt: ['Bebas Neue', 'Helvetica', 'Arial', 'sans-serif'],
            body: ['Roboto', 'Helvetica', 'Arial', 'sans-serif'],
            condensed: ['Roboto Condensed', 'Helvetica', 'Arial', 'sans-serif'],
        },
        fontSize: {
            '2xs': '.65rem',
            'xs': '.75rem',
            'sm': '.875rem',
            'base': '1rem',
            'lg': '1.125rem',
            'xl': '1.3rem',
            '2xl': '1.6rem',
            '3xl': '1.875rem',
            '4xl': '2.45rem',
            '5xl': '3rem',
            '6xl': '4rem',
            '7xl': '5.3rem',
            '8xl': '6.25rem',
            '9xl': '7.5rem',
        },
        fontWeight: {
            light: 300,
            normal: 400,
            medium: 500,
            bold: 700,
        },
        colors: {
            'transparent': 'transparent',
            'black': '#000000',
            'white': '#ffffff',
            'grey': {
                '25': '#fafafa',
                '50': '#f7f7f7',
                '100': '#f3f3f3',
                '125': '#f0f0f0',
                '150': '#ECECEC',
                '175': '#d0d0d0',
                '200': '#ADADAD',
                '300': '#4c4c4c',
                '350': '#4F4F4F',
                '400': '#343434',
                '500': '#303132',
                '600': '#262626',
                '700': '#202020',
                '800': '#1f1f1f',
            },
            'olive': {
                '100': '#d6e8b3'
            },
            'green': {
                '200': '#92c6ab',
                '300': '#76cc9f',
                '400': '#4ca971',
                '500': '#4fc49f',
            },
            'yellow': {
                '100': '#fff7bf',
                '200': '#f7f38a',
                '300': '#ffea5a',
                '400': '#fde119',
                '500': '#f9ce05',
                '600': '#d7b103',
            },
            'red': {
                '600': '#d60d53'
            },
            'brands': {
                'facebook': '#067ceb',
                'twitter': '#00c9ff',
                'gmail': '#ec230e',
                'linkedin': '#0066a9',
            },
            'blue': {
                '100': '#abcdef',
                '200': '#76b4cf',
                '300': '#027da8',
            },
            'turquoise': {
                '200': '#92D2DB',
                '400': '#1BC0E3',
                '500': '#25A5B9'
            },
            'cyan': {
                '100': '#a7d4d1',
                '200': '#57b3bd',
                '300': '#3e8793',
                '400': '#497280',
                '500': '#004958',
            },
            'orange': {
                '100': '#f2d29f',
                '150': '#ffeda5',
                '200': '#f1ae7b',
                '250': '#FEA86A',
                '300': '#ed9654',
            },
            'violet': {
                '100': '#edc8bd',
                '200': '#d28c94',
                '300': '#8d415f',
                '400': '#840048',
                '500': '#670047',
                '600': '#7D337F',
                '700': '#7D347D',
            },
        },
        container: {
            center: true,
            /* @note: needs to be kept in sync with tailwind css/atoms/container.pcss */
            padding: {
                default: '1rem',
                xl: '2rem',
            }
        },
        textDecorationColor: { // defaults to theme => theme('colors')
            'white': '#fff',
            'black': '#000',
        },
    },
    plugins: [
        require('@tailwindcss/typography'),
        require("./tailwind-plugins/buttons")({
            defaultColor: {
                text: 'white',
                background: 'black',
                iconBorder: 'grey.300'
            },
            colors: {
                'black': {
                    text: 'white',
                    background: 'black',
                    iconBorder: 'grey.400',
                    hoverIconBorder: 'grey.600',
                },
                'grey-700': {
                    text: 'white',
                    background: 'grey.700',
                    iconBorder: 'grey.500',
                    hoverBackground: 'grey.400',
                    hoverIconBorder: 'grey.600',
                },
                'grey-500': {
                    text: 'white',
                    background: 'grey.500',
                    iconBorder: 'grey.300',
                    hoverBackground: 'grey.300',
                    hoverIconBorder: 'grey.400',
                },
                'grey-125': {
                    text: 'black',
                    background: 'grey.125',
                },
                'grey-175': {
                    text: 'black',
                    background: 'grey.175',
                },
                'white': {
                    text: 'black',
                    background: 'white',
                    iconBorder: 'grey.100',
                },
                'blue-300': {
                    text: 'white',
                    background: 'blue.300',
                },
                'cyan-200': {
                    text: 'white',
                    background: 'cyan.200',
                },
                'green-300': {
                    text: 'white',
                    background: 'green.300',
                },
                'green-400': {
                    text: 'white',
                    background: 'green.400',
                },
                'green-500': {
                    text: 'black',
                    background: 'green.500',
                },
                'yellow-500': {
                    text: 'black',
                    background: 'yellow.500',
                },
                'yellow-600': {
                    text: 'black',
                    background: 'yellow.600',
                },
                'orange-300': {
                    text: 'white',
                    background: 'orange.300',
                },
                'violet-400': {
                    text: 'white',
                    background: 'violet.400',
                },
                'violet-500': {
                    text: 'black',
                    background: 'violet.500',
                },
                'violet-700': {
                    text: 'black',
                    background: 'violet.700',
                },
                'red-600': {
                    text: 'white',
                    background: 'red.600',
                },
                'brands-facebook': {
                    text: 'white',
                    background: 'brands.facebook',
                }
            }
        })
    ],
}
